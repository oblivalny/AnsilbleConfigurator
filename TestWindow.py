# -*- coding: utf-8 -*-
import yaml
import json
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel

class TestWindow(QWidget):
    def __init__(self, filePath, parent=None):
        super().__init__(parent)
        hbox = QVBoxLayout()
        with (open(filePath, 'r')) as file:
            yamlData=yaml.safe_load(file)
            jsonData=json.dumps(yamlData, indent=4)
        label = QLabel('{0}'.format(jsonData))
        hbox.addWidget(label)
        self.setLayout(hbox)



