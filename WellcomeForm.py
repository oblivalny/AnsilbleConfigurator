# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton
from openFileForm import OpenFileForm
from TestWindow import TestWindow


class WellcomeForm(QWidget):

    def __init__(self):
        super().__init__()
        self.inventory = OpenFileForm('Inventory')
        self.key = OpenFileForm('Key')
        self.openBut = QPushButton("Open Inventory")
        self.vbox = QVBoxLayout()
        self.initUI()

    def initUI(self):
        self.vbox.addWidget(self.inventory)
        self.vbox.addWidget(self.key)
        self.vbox.addWidget(self.openBut)
        self.setLayout(self.vbox)
        self.resize(300, 300)
