# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget
from WellcomeForm import WellcomeForm
from TestWindow import TestWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.openForm = WellcomeForm()
        self.central = QStackedWidget()
        self.initUI()

    def initUI(self):
        self.resize(300, 300)
        self.setWindowTitle('Open')
        self.openForm.openBut.clicked.connect(self.showInventory)
        self.setCentralWidget(self.central)
        self.central.addWidget(self.openForm)
        self.central.setCurrentWidget(self.openForm)
        self.show()

    def showInventory(self):
        print(self.openForm.inventory.path.text())
        inventoryData=TestWindow(self.openForm.inventory.path.text(), self)
        self.central.addWidget(inventoryData)
        self.central.setCurrentWidget(inventoryData)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = MainWindow()
    # tost = testWindow('D:/Workshop/electron/test.yml')
    sys.exit(app.exec_())
