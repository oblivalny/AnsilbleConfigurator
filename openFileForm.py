# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QLineEdit, QPushButton, QFileDialog

class OpenFileForm(QWidget):

    def __init__(self, id):
        super().__init__()

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        self.label = QLabel('{0}: '.format(id))
        self.path = QLineEdit()
        self.openBut = QPushButton("Open")
        self.openBut.clicked.connect(self.getfile)

        hbox.addWidget(self.label)
        hbox.addWidget(self.path)
        hbox.addWidget(self.openBut)

        self.setLayout(hbox)

    def getfile(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            'c:\\', "Yaml (*.yml *.yaml);;All (*)")[0]
        self.path.setText(str(fname))

